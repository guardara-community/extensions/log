# Log Monitor

You can use the [GUARDARA SDK](https://guardara-community.gitlab.io/documentation/docs/developers/sdk) to create a deployable package of this extension. Issue the SDK command below from outside of the directory of this repository:

```
guardara extension -e monitor -o package -n log  
```

You can upload the resulting package (`g-monitor-log.g`) on the Settings / Inventory page of GUARDARA, after which the external engines will pick up and install the extension.

Please note that the built in engine (Destroyer) does not install extensions or run tests. Accordingly, if you wish to test how this extension works, you must first [deploy an external Engine](https://guardara-community.gitlab.io/documentation/docs/deployment#deployment-options) and configure your test to run on this external Engine.
