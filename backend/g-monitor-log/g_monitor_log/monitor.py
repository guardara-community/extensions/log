import copy
import queue
from guardara.sdk.monitor.monitor import MonitorInterface
from g_monitor_log.syslog import Syslog
from g_monitor_log.file import File

class monitor(MonitorInterface):
    def __init__(self, properties, request_queue, response_queue):
        MonitorInterface.__init__(self, properties, request_queue, response_queue)
        self["ready"] = False
        self["state"] = None
        if self.get("method") not in ["File", "Syslog"]:
            raise Exception("Invalid `method` configured for log monitor.")
        self.handler_request_queue = queue.Queue()
        self.handler_response_queue = queue.Queue()
        self.handler = self.__init_handler()

    # -------------------------------------------------------------------------
    # Public Methods
    # -------------------------------------------------------------------------

    def do_start(self):
        self.handler.start()
        return True

    def do_stop(self):
        self.handler.stop()
        self.handler.join()
        return True

    def do_status(self):
        report = copy.deepcopy(self.get("state"))
        self["state"] = None
        return report

    def do_is_ready(self):
        return self.get("ready")

    def custom_loop(self):
        response = self.__get_handler_response()
        if response.get("command") == "exception":
            raise response.get("exception")
        if response.get("command") == "set":
            self[response.get("key")] = response.get("value")

    def do_restart(self):
        pass

    # -------------------------------------------------------------------------
    # Private Methods
    # -------------------------------------------------------------------------

    def __get_handler_response(self):
        response = None
        try:
            response = self.handler_response_queue.get(timeout=0.001)
            self.handler_response_queue.task_done()
        except queue.Empty:
            pass
        return response

    def __init_handler(self):
        patterns = self.get("patterns").encode("utf-8")
        if self.get("method") == "File":
            return File(
                self.handler_request_queue,
                self.handler_response_queue,
                self.get("path"),
                patterns,
            )
        if self.get("method") == "Syslog":
            return Syslog(
                self.handler_request_queue,
                self.handler_response_queue,
                self.get("protocol"),
                self.get("port"),
                patterns,
                self.get("source"),
            )
