import copy
import socket
from g_monitor_log.source import Source
from g_monitor_log.processor import Processor

class Syslog(Source):

    def __init__(self, request, response, protocol, port, patterns, source):
        Source.__init__(self, request, response)
        self.protocol = protocol
        self.port = port
        self.patterns = patterns
        self.source = source
        self.read_buffer = b''

    # -------------------------------------------------------------------------
    # Public Methods
    # -------------------------------------------------------------------------

    def execute(self):
        self.__prepare_socket()
        self.socket.bind(('0.0.0.0', self.port))
        if self.protocol == 'TCP':
            self.__run_tcp(self.source, self.patterns)
        else:
            self.__run_udp(self.source, self.patterns)
        self.__cleanup()

    # -------------------------------------------------------------------------
    # Private Methods
    # -------------------------------------------------------------------------

    def __prepare_socket(self):
        if self.protocol == 'TCP':
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if self.protocol == 'UDP':
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def __cleanup(self):
        if self.socket is None:
            return
        self.socket.close()
        self.socket = None

    def __read_line(self, sock):
        data = b''
        if len(self.read_buffer) > 0:
            newline_pos = self.read_buffer.find(b"\n")
            if newline_pos != -1:
                data += copy.deepcopy(self.read_buffer[0:newline_pos])
                self.read_buffer = self.read_buffer[newline_pos + 1:]
                return data

        data += copy.deepcopy(self.read_buffer)
        self.read_buffer = b''

        sock.settimeout(0.5)
        while True and not self.should_stop.is_set():
            blist = b''

            try:
                blist = sock.recv(1024)
                if len(blist) == 0:
                    return None
            except socket.timeout:
                continue
            except socket.error:
                return None

            blist = blist.replace(b"\r\n", b"\n")
            newline_pos = blist.find(b"\n")
            if newline_pos == -1:
                data += blist
                continue
            data += blist[0:newline_pos]
            self.read_buffer = blist[newline_pos + 1:]
            return data

    def __run_tcp(self, source, patterns):
        self.socket.listen(1)
        self.socket.settimeout(0.01)
        self.ready(True)
        while not self.should_stop.is_set():
            conn = None
            addr = None
            try:
                conn, addr = self.socket.accept()
            except socket.timeout:
                continue
            if source is not None and len(source) > 0 and source != addr[0]:
                conn.close()
                break
            while not self.should_stop.is_set():
                line = None
                self.c_action = None
                self.state = None
                line = self.__read_line(conn)
                if line is None:
                    try:
                        conn.close()
                    except:
                        pass
                    break
                result = Processor.check_match(patterns, line)
                if result is not True:
                    continue
                report = Processor.prepare_report(line)
                self.report(report)
        self.ready(False)

    def __run_udp(self, source, patterns):
        self.socket.settimeout(0.01)
        self.ready(True)
        while not self.should_stop.is_set():
            line = None
            addr = None
            try:
                line, addr = self.socket.recvfrom(65535)
            except socket.timeout:
                continue
            if source is not None and len(source) > 0 and source != addr[0]:
                continue
            if line is None:
                continue
            result = Processor.check_match(patterns, line)
            if result is not True:
                continue
            report = Processor.prepare_report(line)
            self.report(report)
        self.ready(False)
