import threading
from g_monitor_log.processor import Processor

class Source(threading.Thread):

    def __init__(self, request, response):
        threading.Thread.__init__(self)
        self.request = request
        self.response = response
        self.should_stop = threading.Event()

    def stop(self):
        self.should_stop.set()

    def execute(self):
        raise Exception("Log monitor source should implement run method.")

    def ready(self, value):
        self.response.put({
            "command": "set",
            "key": "ready",
            "value": value
        })

    def report(self, value):
        self.response.put({
            "command": "set",
            "key": "state",
            "value": value
        })

    def run(self):
        try:
            self.execute()
        except Exception as ex:
            self.response.put({
                "command": "exception",
                "exception": ex
            })
