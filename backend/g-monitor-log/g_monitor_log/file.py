import time
from g_monitor_log.source import Source
from g_monitor_log.processor import Processor

class File(Source):

    def __init__(self, request, response, path, patterns):
        Source.__init__(self, request, response)
        self.path = path
        self.patterns = patterns
        self.fd = None

    # -------------------------------------------------------------------------
    # Public Methods
    # -------------------------------------------------------------------------

    def execute(self):
        self.__open_log_file()
        self.ready(True)
        while not self.should_stop.is_set():
            line = self.fd.readline()
            if not line or len(line) == 0:
                continue
            result = Processor.check_match(self.patterns, line)
            if result is not True:
                continue
            report = Processor.prepare_report(line)
            self.report(report)
        self.ready(False)
        self.__cleanup()

    # -------------------------------------------------------------------------
    # Private Methods
    # -------------------------------------------------------------------------

    def __open_log_file(self):
        self.fd = None
        retry = 0
        while self.fd is None:
            try:
                self.fd = open(self.path, 'rb')
            except FileNotFoundError:
                if retry > 5:
                    raise Exception("Log file could not be found.")
                time.sleep(1)
                retry += 1
            except Exception as ex:
                print(str(ex))
        retry = 0
        self.fd.seek(0, 2)

    def __cleanup(self):
        if self.fd is None:
            return
        self.fd.close()
        self.fd = None
