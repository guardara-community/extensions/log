import re

class Processor:

    @staticmethod
    def check_match(patterns, line):
        patterns = patterns.replace(b"\r\n", b"\n")
        for pattern in patterns.split(b"\n"):
            if re.match(pattern, line):
                return True
        return False

    @staticmethod
    def prepare_report(line):
        report = b"The following log entry matched one of the detection patterns configured:\n\n%s\n" % line
        return report.decode("utf-8")
