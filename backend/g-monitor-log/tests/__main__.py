import unittest

def prepare_load_tests_function(path):
    test_suite = unittest.TestLoader().discover(path)
    def load_tests (_a, _b, _c):
        return test_suite
    return load_tests

load_tests = prepare_load_tests_function(".")
unittest.main()
