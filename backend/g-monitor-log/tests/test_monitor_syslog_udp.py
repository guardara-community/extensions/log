import unittest

import time
import queue
import socket
import threading
from g_monitor_log.monitor import monitor as Monitor

class SyslogClient(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.address = '127.0.0.1'
        self.port = 6666

    def run(self):
        time.sleep(2)
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.sendto(b"TEST\r\n", (self.address, self.port))
        s.sendto(b"This is a USER_PROCESS: just a test\n", (self.address, self.port))
        s.close()

class FileClient(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.path = "/tmp/log_monitor_file_test.txt"

    def run(self):
        c = 0
        until = time.time() + 10
        f = open(self.path, "wb+")
        while time.time() < until:
            if c % 10 == 0:
                f.write(b"This is a USER_PROCESS: just a test\r\n")
            else:
                f.write(b"TEST\r\n")
            c += 1
            time.sleep(0.5)
        f.close()

# -----------------------------------------------------------------------------
# Syslog Test
# -----------------------------------------------------------------------------

request_queue = queue.Queue()
response_queue = queue.Queue()

class TestMonitorSyslogUdp(unittest.TestCase):

    @staticmethod
    def send(request):
        request_queue.put(request)
        r = response_queue.get()
        response_queue.task_done()
        return r

    def test_works(self):
        monitor = Monitor({
            "method": "Syslog",
            "port": 6666,
            "protocol": "UDP",
            "patterns": ".*USER_PROCESS:.*"
        }, request_queue, response_queue)

        client = SyslogClient()

        monitor.start()
        client.start()
        r = TestMonitorSyslogUdp.send({ "command": "start" })
        self.assertEqual(r.get("command"), "start")
        self.assertEqual(r.get("response"), True)

        ready = False
        while not ready:
            r = TestMonitorSyslogUdp.send({ "command": "is_ready" })
            ready = r.get("response")

        while True:
            r = TestMonitorSyslogUdp.send({ "command": "status" })
            if r.get('response') is not None:
                self.assertEqual(r.get("command"), "status")
                self.assertEqual(r.get("response"), "The following log entry matched one of the detection patterns configured:\n\nThis is a USER_PROCESS: just a test\n\n")
                break

        monitor.stop()
        monitor.shutdown()
        client.join()
        monitor.join()
