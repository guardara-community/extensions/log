import unittest

from g_monitor_log.processor import Processor

class TestProcessor(unittest.TestCase):

    def test_check_match(self):
        result = Processor.check_match(b".*unittest.*", b"This is a unittest here.")
        self.assertEqual(result, True)
        result = Processor.check_match(b".*unittest.*", b"This is a test.")
        self.assertEqual(result, False)

    def test_prepare_report(self):
        result = Processor.prepare_report(b"unittest")
        self.assertEqual(result, "The following log entry matched one of the detection patterns configured:\n\nunittest\n")
