import unittest

import os
import time
import queue
import threading
from g_monitor_log.monitor import monitor as Monitor

class FileClient(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.path = "/tmp/log_monitor_file_test.txt"

    def run(self):
        c = 0
        until = time.time() + 2
        f = open(self.path, "wb+")
        while time.time() < until:
            if c % 10 == 0:
                f.write(b"This is a USER_PROCESS: just a test\n")
            else:
                f.write(b"TEST\n")
            c += 1
            time.sleep(0.5)
        f.close()

# -----------------------------------------------------------------------------
# File Test
# -----------------------------------------------------------------------------

request_queue = queue.Queue()
response_queue = queue.Queue()

class TestMonitorFile(unittest.TestCase):

    @staticmethod
    def send(request):
        request_queue.put(request)
        r = response_queue.get()
        response_queue.task_done()
        return r

    def test_works(self):
        monitor = Monitor({
            "method": "File",
            "path": "/tmp/log_monitor_file_test.txt",
            "patterns": ".*USER_PROCESS:.*"
        }, request_queue, response_queue)

        client = FileClient()

        monitor.start()
        client.start()

        r = TestMonitorFile.send({ "command": "start" })
        self.assertEqual(r.get("command"), "start")
        ready = False
        while not ready:
            r = TestMonitorFile.send({ "command": "is_ready" })
            ready = r.get("response")

        while True:
            r = TestMonitorFile.send({ "command": "status" })
            if r.get('response') is not None:
                self.assertEqual(r.get("response"), "The following log entry matched one of the detection patterns configured:\n\nThis is a USER_PROCESS: just a test\n\n")
                break

        monitor.stop()
        monitor.shutdown()
        client.join()
        monitor.join()
        os.remove("/tmp/log_monitor_file_test.txt")
